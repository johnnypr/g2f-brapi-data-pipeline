from pathlib import Path
import requests

def main():
    ''' Fetches the ontology data for maize from cropontology.org and saves it locally '''

    fetch_url = "http://www.cropontology.org/get-ontology/CO_322"
    directory = Path("./ontology_data")
    directory.mkdir(parents=True, exist_ok=True)

    response = requests.get(fetch_url)

    write_path = directory / "CO_322.json"

    with write_path.open(mode="w") as write_handle:
        write_handle.write(response.text)

if __name__ == "__main__":
    main()