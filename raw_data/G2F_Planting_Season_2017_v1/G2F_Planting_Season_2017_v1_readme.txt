/Bridget_McFarland_G2F_Aug_2019
	G2F_Planting_Season_2017_V2_readme.txt (this file)

/a._2017_hybrid_phenotypic_data: This directory contains 2 files
	
	1. g2f_2017_hybrid_data_clean.csv
	   Plot level phenotypic traits gathered in hybrid experiments in 2017, with outliers removed

	2. g2f_2017_hybrid_data_raw.csv
	   Plot level phenotypic traits gathered, in hybrid experiments in 2017, including outliers

/b._2017_weather_data: This directory contains 1 file
	
	1. g2f_2017_weather_data.csv
	   Location specific weather measurements

/c._2017_soil_data: This directory contains 3 files
	
	1. g2f_2017_soil_data_clean.csv
	   Plot level phenotypic traits gathered, in inbred experiments, with outliers removed

	2. g2f_2017_soil_data_raw.csv
	   Plot level phenotypic traits gathered, in inbred experiments, including outliers

	3. g2f_2017_soil_data.txt
	   Data columns and descriptions of the csv files in this directory

/d._2017_genotypic_data: This directory contains 5 files

	1. g2f_2017_gbs_hybrid_codes.xlsx
	   GBS codes for the pedigrees that were genotyped

	2. g2f_2017_ZeaGBSv27_Imputed_AGPv4.h5
	   GBS imputed data formatted for Tassel 5 and lifted to version 4 of the B73 reference genome, non-compressed

	3. g2f_2017_ZeaGBSv27_Imputed_AGPv4.h5.zip
	   GBS imputed data formatted for Tassel 5 and lifted to version 4 of the B73 reference genome, compressed (software available here: http://dx.doi.org/10.7946/P2X597)

	4. g2f_2017_ZeaGBSv27_Raw_AGPv4.h5
	   GBS raw data formatted for Tassel 5 lifted to version 4 of the B73 reference genome, non-compressed 

	5. g2f_2017_ZeaGBSv27_Raw_AGPv4.h5.zip
	   GBS raw data formatted for Tassel 5 and lifted to version 4 of the B73 reference genome, compressed (software available here: http://dx.doi.org/10.7946/P2X597)

/z._2017_supplemental_info: This directory contains 4 files

	1. g2f_2017_agronomic_information.csv
	   Location-specific notes of coordinates, inputs and weather station ID
	
	2. g2f_2017_cooperators_list.csv
	   List of the cooperators and their trial locations

	3. g2f_2017_field_metadata.csv
	   Trial location metadata

	4. g2f_2017_supplemental_information.txt
	   Data columns and descriptions of the csv files in this directory

For general information or questions about the Genomes to Fields (G2F) initiative please contact ndeleongatti@wisc.edu

For questions regarding released datasets contact:
	GBS:		Cinta Romay		mcr72@cornell.edu
	Inbred/Hybrids:	Bridget McFarland	bamcfarland@wisc.edu
	Weather:	Christina Poudyal	cpoudyal@umn.edu

***Genomes to Fields***
	Initiative co-leads:
		Pat Schnable, Iowa State University
		Natalia de Leon, University of Wisconsin - Madison
	Data wranglers:
		Naser AlKhalifah, Darwin Campbell, Jode Edwards, David Ertl, Joseph Gage, Jack Gardiner, James Holland, Carolyn Lawrence-Dill, Dayane Lima, Bridget A. McFarland, Christina Poudyal, Anna Rogers, Cinta Romay, Emily Rothfusz, Kevin Silverstein, Ramona Walls, Ching-Ting Yeh





