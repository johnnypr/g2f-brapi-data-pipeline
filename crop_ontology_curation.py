import requests
import sys
import json
from configparser import ConfigParser

def getCurationData():
    r = requests.get("http://www.cropontology.org/get-ontology/CO_322")
    jsonData = json.loads(r.text)
    print(type(jsonData))
    print(jsonData.keys())

def parseInitFile(config_path):
    config = ConfigParser()
    config.read(config_path)

    Fields = config.get("ONTOLOGIES","Fields")
    getCurationData()

if __name__ == '__main__':
    config_path = sys.argv[1]
    parseInitFile(config_path)

