import json
import arrow


def make_crop_fixture(crop_name):
    data = {
        "model": "brapi.Crop",
        "fields": {
            "cropdbid": crop_name,
            "commonname": crop_name
        }
    }
    return data

def make_ontology_fixture(ontology_name, crop_name):

    data = {
        "model": "brapi.Ontology",
        "fields": {
            "cropdbid": crop_name,
            "ontologyDbId": ontology_name,
            "ontologyName": ontology_name
        }
    }

    return data



def make_obs_variable_data_type_fixtures(crop_name):

    data = []

    datatype_map = {
        'Numeric': 1
        , 'Numerical': 1
        , 'Categorical': 2
        , 'Date': 3
        , 'Text': 4
        , 'Picture': 5
        , 'Boolean': 6
        , 'Time': 7
    }

    for k, v in datatype_map.items():

        data.append({
            "model": "brapi.ObservationVariableDatatype",
            "fields": {
                "cropdbid": crop_name,
                "observationvariabledatatypedbid": int(v),
                "data": k
            }
        })

    return data


def make_programs_fixtures():
    return [
        {
            "model": "brapi.Program",
            "fields": {
                "cropdbid": "maize",
                "programDbId": "G2F Hybrids",
                "name": "G2F Hybrids",
                "abbreviation": None,
                "objective": None,
                "leadPerson": None
            }
        },
        {
            "model": "brapi.Program",
            "fields": {
                "cropdbid": "maize",
                "programDbId": "G2F Inbreds",
                "name": "G2F Inbreds",
                "abbreviation": None,
                "objective": None,
                "leadPerson": None
            }
        }
    ]


def make_boilerplate_fixtures(crop_name, ontology_name):

    data = []
    data.append(make_crop_fixture(crop_name))
    data.append(make_ontology_fixture(ontology_name, crop_name))
    data = data + make_obs_variable_data_type_fixtures(crop_name) + make_programs_fixtures()
    return data


def trials_parser():
    output_trials = []

    MODEL = 'brapi.Trial'
    CROP = 'maize'
    PROGRAMS = ['G2F Hybrids', 'G2F Inbreds']
    ACTIVE = False

    TRIALS = ('2014', '2015', '2016', '2017')



    for program_name in PROGRAMS:

        for base_trial_name in TRIALS:

            trial = {
                'model': MODEL,
                'fields': {
                    'cropdbid': CROP,
                    'programDbId': program_name,
                    'trialDbId': f'{program_name} - {base_trial_name}',
                    'trialName': f'{program_name} - {base_trial_name}',
                    'startDate': None,
                    'endDate': None,
                    'active': ACTIVE,
                    'datasetAuthorshipLicence': None,
                    'datasetAuthorshipDatasetPUI': None
                }
            }

            output_trials.append(trial)

    return output_trials



def seasons_parser(crop, year, season="summer"):

    season_fixtures = []
    study_fixtures = []

    season_fixtures.append({
        'model': 'brapi.Season',
        'fields': {
           'cropdbid': crop,
            'seasonDbId': f'{year}_{season}',
            'year': year,
            'season': season
        }
    })
    '''
    for study in studies:

        #year = study.get("fields").get("trialDbId").split(" - ")[-1].strip()

        study_fixtures.append({
            "model": "brapi.StudySeason",
            "fields": {
                "cropdbid": crop,
                "studyDbId": study.get("pk"),
                "seasonDbId": f'{year}_{season}',
            }
        })
        
    '''

    return season_fixtures



def studies_parser(input_filepath, crop_name, trial_name, study_postfix):

    MODEL = 'brapi.Study'
    ACTIVE = False
    LICENSE = None
    studies = []
    study_ids = []

    with open(input_filepath, 'r') as input_handle:

        input_data = json.loads(input_handle.read())

        for line in input_data:

            _id = line.get('Field-Location') or line.get('Location')

            if _id in study_ids:
                continue

            study = {
                'model': MODEL,
                'pk': _id + study_postfix,
                'fields': {
                    'cropdbid': crop_name,
                    'trialDbId': trial_name,
                    'locationDbId': _id,
                    'studyType': None,
                    'studyName': _id,
                    'studyDescription': None,
                    'startDate': None,
                    'endDate': None,
                    'active': ACTIVE,
                    'license': LICENSE,
                    'lastUpdateVersion': None,
                    'lastUpdateTimestamp': None
                }
            }

            study_ids.append(_id)
            studies.append(study)

        return studies


def germplasm_parser(input_filepath, crop_fk):
    with open(input_filepath, 'r') as input_handle:

        germplasm_list = []
        processed_ids = []
        input_data = json.loads(input_handle.read())

        for row in input_data:

            _id = row.get('Pedigree')

            if _id in processed_ids:
                continue

            germ = {
                'model': 'brapi.Germplasm',
                'pk': _id,
                'fields': {
                    'cropdbid': crop_fk,
                    'germplasmName': _id,
                    'defaultDisplayName': _id,
                    'pedigree': _id,
                    'instituteCode': ''
                }
            }

            germplasm_list.append(germ)
            processed_ids.append(_id)

        return germplasm_list



def observation_units_parser(input_filepath, study_id_postfix=None, OUT_FILEPATH=None):
    MODEL_STR = 'brapi.ObservationUnit'
    CROP = 'maize'
    PROGRAM = 'G2F Hybrids'
    #STUDY = 'DEH1'

    if study_id_postfix is None:
        study_id_postfix = ''

    key_map = {
        'germplasmDbId': 'Pedigree',
        #'observationUnitDbId': 'RecId',
        'blockNumber': 'Block',
        'plotNumber': 'Plot',
    }

    obs_units = []

    with open(input_filepath, 'r') as input_handle:

        input_data = json.loads(input_handle.read())

        for line in input_data:

            block = line.get('Block')
            plot = line.get('Plot')

            if block is None or len(block) == 0:
                block = 'UnknownBlock'

            if plot is None or len(plot) == 0:
                plot = 'UnknownPlot'

            _study = line.get('Field-Location') or line.get('Location')
            _study = _study + study_id_postfix

            if _study is None:
                _study = line.get('Location') + study_id_postfix

            _id = '-'.join([_study, block, plot]) #line.get('Block'), line.get('Plot')])


            unit_id = line.get('RecId') or line.get('Code')

            unit = {
                'model': MODEL_STR,
                'pk': unit_id,
                'fields': {
                    'cropdbid': CROP,
                    'programDbId': PROGRAM,
                    'studyDbId': _study, #STUDY,
                    'observationLevel': 'plot',
                    'observationUnitName': _id
                }
            }

            for key1, key2 in key_map.items():
                unit['fields'][key1] = line.get(key2)

            obs_units.append(unit)

        if OUT_FILEPATH:
            with open(OUT_FILEPATH, 'w') as output_handle:
                output_handle.write(json.dumps(obs_units, indent=3))

    return obs_units


def get_observation_date(date_str):
    print("WHAT IS date_str?", date_str)

    if date_str is None or len(date_str) == 0:
        return None

    date_value = None
    # TODO: Better datetime representation in the DB!
    try:
        date_value = arrow.get(date_str, 'M/D/YYYY')
    except arrow.parser.ParserError:
        date_value = arrow.get(date_str, 'M/D/YY')

    return date_value


def observations_parser(input_filepath, crop):
    MODEL = 'brapi.Observation'
    ACTIVE = False
    LICENSE = None
    observations = []
    counter = 1

    SILKING_DAP_FK = 'Silking time' # (days from planting)'
    ANTHESIS_DAP_FK = 'Anthesis time' # (days from planting)'
    PLANT_HEIGHT_FK = 'Plant height'
    EAR_HEIGHT_FK = 'Ear height'
    GRAIN_MOISTURE_FK = 'Grain moisture'

    with open(input_filepath, 'r') as input_handle:

        input_data = json.loads(input_handle.read())

        for line in input_data:

            #planting_date = line.get('Date Plot Planted') or line.get('Planting Date [MM/DD/YY]')
            planting_date_str = line.get('Date Plot Planted') or line.get('Planting Date [MM/DD/YY]') or line.get('Date Planted')
            planting_date = get_observation_date(planting_date_str)


            if planting_date is None:
                # Todo: log the non-parsable records to some file...
                continue


            row_id = line.get('RecId') or line.get('Code')


            harvest_date_str = line.get('Date Harvested') or line.get('Harvest Date [MM/DD/YYYY]') or line.get('Harvest Date [MM/DD/YY]') or line.get('Date Plot Harvested')
            harvest_date = get_observation_date(harvest_date_str)



            # PROCESS GRAIN MOISTURE:
            grain_moisture_value = line.get('Grain Moisture [percent]')

            if grain_moisture_value is not None and harvest_date is not None:
                obs = {
                    'model': MODEL,
                    'pk': f'{row_id}_grain_moisture',
                    'fields': {
                        'cropdbid': crop,
                        'observationUnit': row_id,
                        'obsVariable': GRAIN_MOISTURE_FK,
                        'observationTimestamp': harvest_date.isoformat(),
                        'value': grain_moisture_value
                    }
                }
                observations.append(obs)

            # PROCESS PLANT HEIGHT:
            plant_height_value = line.get('Plant Height [cm]') or line.get('Plant height [cm]') or line.get('PHT (cm)')


            if harvest_date is not None and plant_height_value is not None:
                obs = {
                    'model': MODEL,
                    'pk': f'{row_id}_plant_height',
                    'fields': {
                        'cropdbid': crop,
                        'observationUnit': row_id,
                        'obsVariable': PLANT_HEIGHT_FK,
                        'observationTimestamp': harvest_date.isoformat(),
                        'value': plant_height_value
                    }
                }
                observations.append(obs)


            # PROCESS EAR HEIGHT:
            ear_height_value = line.get('Ear Height [cm]') or line.get('Ear height [cm]') or line.get('EHT (cm)')

            if harvest_date is not None and ear_height_value is not None:
                obs = {
                    'model': MODEL,
                    'pk': f'{row_id}_ear_height',
                    'fields': {
                        'cropdbid': crop,
                        'observationUnit': row_id,
                        'obsVariable': EAR_HEIGHT_FK,
                        'observationTimestamp': harvest_date.isoformat(),
                        'value': ear_height_value
                    }
                }
                observations.append(obs)


            # PROCESS ANTHESIS DAP
            anthesis_value = line.get('Pollen DAP [days]') or line.get('Pollen DAP')
            anthesis_date_value = line.get('Anthesis [date]', None) or line.get('DMF', None) or line.get('Pollen DAP')
            anthesis_date_str = line.get('Anthesis [date]', None) or line.get('DMF', None) or line.get('Anthesis [days]')

            if anthesis_date_value is None or len(anthesis_date_value) == 0:
                anthesis_date_value = None

            if anthesis_date_value is not None: # and planting_date is not None:
                # TODO: Better datetime representation in the DB!
                anthesis_date_value = get_observation_date(anthesis_date_str)

                ''' 
                try:
                    anthesis_date_value = arrow.get(anthesis_date_str, 'M/D/YYYY')
                except arrow.parser.ParserError:
                    anthesis_date_value = arrow.get(anthesis_date_str, 'M/D/YY')

                if anthesis_value is None:
                    # Need to parse the days to anthesis ourselves (see 2014 inbred data):
                    anthesis_value = (anthesis_date_value - planting_date).days
                '''

                try:
                    obs = {
                        'model': MODEL,
                        'pk': f'{row_id}_anthesis_dap',
                        'fields': {
                            'cropdbid': crop,
                            'observationUnit': row_id,
                            'obsVariable': ANTHESIS_DAP_FK,
                            'observationTimestamp': anthesis_date_value.isoformat(),
                            'value': anthesis_value
                        }
                    }
                    observations.append(obs)
                except arrow.parser.ParserError:
                    pass
                    #print('COULD NOT PARSE ANTHESIS DATE:', line.get('Anthesis [date]'))
                    #anthesis_date = None


            # PROCESS SILKING DAP
            silking_dap_value = line.get('Silk DAP [days]') or line.get('Silk DAP')
            silking_date_str = line.get('Silking [date]') or line.get('DFF') or line.get('Silking [days]')

            if silking_date_str is not None and len(silking_date_str) != 0:

                # TODO: Better datetime representation in the DB!
                silking_date = get_observation_date(silking_date_str)
                #silking_date = arrow.get(silking_date_str, 'M/D/YYYY')

                if silking_dap_value is None:
                    # Calculate the silking days if it is not present in the data:
                    print(silking_date, planting_date)
                    silking_dap_value = (silking_date - planting_date).days

                obs = {
                    'model': MODEL,
                    'pk': f'{row_id}_silking_dap',
                    'fields': {
                        'cropdbid': crop,
                        'observationUnit': row_id,
                        'obsVariable': SILKING_DAP_FK,
                        'observationTimestamp': silking_date.isoformat(),
                        'value': silking_dap_value
                    }
                }
                observations.append(obs)

            counter += 1


    return observations



def observation_variables_parser():
    ontology_ids_str = '''Trait ID	Trait Method	Trait FAIR
Plant Height    CO_322:0000994	CO_322:0000995 CO_322:0000348
Ear height  CO_322:0000017	CO_322:0000179 CO_322:0000348
CO_322:0001091	CO_322:0001092	
CO_322:0000015	CO_322:0000349	
CO_322:0001091	CO_322:0001092	
CO_322:0000167	CO_322:0000221	
CO_322:0000008	CO_322:0000201	
CO_322:0000179	CO_322:0000569	
CO_322:0000035	CO_322:0000570	
CO_322:0000031	CO_322:0000905	CO_322:0000191
CO_322:0000030	CO_322:0000795	CO_322:0000189
CO_322:0000033	CO_322:0000195	
CO_322:0001005	CO_322:0001006	
CO_322:0000005	CO_322:0000217'''

    lines = ontology_ids_str.split("\n")
    terms = [line.rstrip().split("\t") for line in lines[1:]]
    obs_variables = []

    ONTOLOGY_ID = 'CO_322'

    with open('./ontology_term_map.json', 'r') as map_handle, open('ontology_data/CO_322.json', 'r') as ontology_lookup_handle:

        ontology_terms = json.loads(map_handle.read())
        ontology_lookup = json.loads(ontology_lookup_handle.read())

        #print(json.dumps(ontology_terms, indent=3))

        for term in ontology_terms:
            #print(term)

            trait_id = term.get('trait_id')
            method_id = term.get('method_id')
            scale_id = term.get('scale_id')
            #print("WHAT IS SCALE ID?", scale_id)
            trait_name = ontology_lookup.get(trait_id).get('name').get('english')
            method = ontology_lookup.get(trait_id).get('method').get(method_id)



            obs_variables.append({
                'model': 'brapi.Method',
                'fields': {
                    'cropdbid': 'maize',
                    'methodDbId': method_id,
                    'name': method.get('name').get('english'),
                    'description': method.get('definition').get('english')

                }
            })


            method_name = method.get('name').get('english')

            if scale_id:

                try:

                    scale = method.get('scale').get(scale_id)
                    scale_name = scale.get('scale_name').get('english')
                    scale_type = scale.get('type').get('english')
                    scale_min = scale.get('min', {}).get('english')
                    scale_max = scale.get('max', {}).get('english')
                    #print("WHAT IS SCALE?")
                    #print(scale_name, scale_type, scale_min, scale_max)

                    '''
                    The Data Type DB IDs:
                   {
                      "model": "brapi.ObservationVariableDatatype",
                      "fields": {
                         "cropdbid": "maize",
                         "observationvariabledatatypedbid": 1,
                         "data": "Numeric"
                      }
                   },
                   {
                      "model": "brapi.ObservationVariableDatatype",
                      "fields": {
                         "cropdbid": "maize",
                         "observationvariabledatatypedbid": 1,
                         "data": "Numerical"
                      }
                   },
                   {
                      "model": "brapi.ObservationVariableDatatype",
                      "fields": {
                         "cropdbid": "maize",
                         "observationvariabledatatypedbid": 2,
                         "data": "Categorical"
                      }
                   },
                   {
                      "model": "brapi.ObservationVariableDatatype",
                      "fields": {
                         "cropdbid": "maize",
                         "observationvariabledatatypedbid": 3,
                         "data": "Date"
                      }
                   },
                   {
                      "model": "brapi.ObservationVariableDatatype",
                      "fields": {
                         "cropdbid": "maize",
                         "observationvariabledatatypedbid": 4,
                         "data": "Text"
                      }
                   },
                   {
                      "model": "brapi.ObservationVariableDatatype",
                      "fields": {
                         "cropdbid": "maize",
                         "observationvariabledatatypedbid": 5,
                         "data": "Picture"
                      }
                   },
                   {
                      "model": "brapi.ObservationVariableDatatype",
                      "fields": {
                         "cropdbid": "maize",
                         "observationvariabledatatypedbid": 6,
                         "data": "Boolean"
                      }
                   },
                  {
                      "model": "brapi.ObservationVariableDatatype",
                      "fields": {
                         "cropdbid": "maize",
                         "observationvariabledatatypedbid": 7,
                         "data": "Time"
                      }
                   },
                    
                    '''

                    datatype_map = {
                        'Numeric': 1
                        , 'Numerical': 1
                        , 'Categorical': 2
                        , 'Date': 3
                        , 'Text': 4
                        , 'Picture': 5
                        , 'Boolean': 6
                        , 'Time': 7
                    }

                    scale = {
                        'model': 'brapi.Scale',
                        'fields': {
                            'cropdbid': 'maize',
                            'scaleDbId': scale_id,
                            'name': scale_name,
                            'datatypeDbId': datatype_map.get(scale_type)
                        }
                    }

                    if scale_min and scale_max:

                        valid_value_id = f'{scale_id}_valid_value'

                        valid_value = {
                            'model': 'brapi.ValidValue',
                            'fields': {
                                'cropdbid': 'maize',
                                'min': float(scale_min),
                                'max': float(scale_max),
                                'validValueDbId': valid_value_id
                            }
                        }
                        obs_variables.append(valid_value)

                        scale['fields']['validValues'] = valid_value_id


                    obs_variables.append(scale)



                except AttributeError as e:
                    print(e)
                    print("TERM FROM LOCAL DATA:")
                    print(term)
                    print("")
                    print("FROM ONTOLOGY:")
                    pprint(ontology_lookup.get(trait_id))
                    print("BAD SCALE DATA?")
                    pprint(scale)
                    print("-" * 50)



            obs_variables.append({
                'model': 'brapi.ObservationVariable',
                'fields': {
                    'cropdbid': 'maize',
                    'ontologyDbId': ONTOLOGY_ID,
                    'observationVariableDbId': term.get('name'), #term_def.get('name').get('english'),  # counter,
                    'observationVariableName': term.get('name'), #term_def.get('name').get('english'),
                    'traitDbId': trait_id, #term[0],
                    'methodDbId': method_id, #term[1],
                    # 'method': term_def.get('method').get(term[1])
                }
            })

        return obs_variables
