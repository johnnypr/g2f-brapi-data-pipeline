import csv
import json

class ObservationParser:

    def __init__(self, filepath):

        self.filepath = filepath

        # Keys derived from the file _2016_hybrid_phenotypic_data / g2f_2016_hybrid_data_raw
        self.keys_of_interest = ['RecId', 'Field-Location', 'Line Grouping', 'Source', 'Pedigree', 'Plot length [field]', 'Alley length',
                                 'Row spacing', 'Plot area', 'Rows per packet', '# Seed', 'Entry', 'Replicate', 'Plot', 'Range', 'Pass',
                                 'Date Plot Planted', 'Date Plot Harvested', 'Anthesis [date]', 'Silking [date]', 'Pollen DAP [days]',
                                 'Silk DAP [days]', 'Plant Height [cm]', 'Ear Height [cm]', 'Stand Count [plants]', 'Root Lodging [plants]',
                                 'Stalk Lodging [plants]', 'Grain Moisture [percent]', 'Test Weight [lbs/bu]', 'Plot Weight [lbs]',
                                 'Grain Yield [bu/acre]', 'Plot Discarded', 'Filler', 'Comments']


    def read(self):

        with open(self.filepath) as csvfile:
            reader = csv.DictReader(csvfile)
            rows = list(reader)
            return rows



if __name__ == '__main__':


    INPUT_FILEPATH = './raw_data/Carolyn_Lawrence_Dill_G2F_Nov_2016_V.3/a._2014_hybrid_phenotypic_data/g2f_2014_hybrid_raw.csv'
    OUTPUT_FILEPATH = './json/g2f_2014_hybrid_data.json'

    # This looks to be the worst of the files:
    INPUT_FILEPATH = './raw_data/Carolyn_Lawrence_Dill_G2F_Mar_2017/d._2015_inbred_phenotypic_data/g2f_2015_inbred_raw_data.csv'
    OUTPUT_FILEPATH = './json/g2f_2015_inbred_data.json'

    # Thanks for the incosistent file naming!!!
    INPUT_FILEPATH = './raw_data/Carolyn_Lawrence_Dill_G2F_Mar_2017/a._2015_hybrid_phenotypic_data/g2f_2015_hybrid_data_raw.csv'
    OUTPUT_FILEPATH = './json/g2f_2015_hybrid_data.json'


    INPUT_FILEPATH = './raw_data/Carolyn_Lawrence_Dill_G2F_Nov_2016_V.3/d._2014_inbred_phenotypic_data/g2f_2014_inbred_raw.csv'
    OUTPUT_FILEPATH = './json/g2f_2014_inbred_data.json'


    INPUT_FILEPATH = './raw_data/GenomesToFields_G2F_2016_Data_Mar_2018/a._2016_hybrid_phenotypic_data/g2f_2016_hybrid_data_raw.csv'
    OUTPUT_FILEPATH = './json/g2f_2016_hybrid_data.json'


    #INPUT_FILEPATH = "./raw_data/2017/g2f_2017_hybrid_data_raw.csv"
    #OUTPUT_FILEPATH = "./json/g2f_2017_hybrid_data.json"


    parser = ObservationParser(INPUT_FILEPATH)
    data = parser.read()

    print(f'writing {len(data)} lines to {OUTPUT_FILEPATH}')

    with open(OUTPUT_FILEPATH, "w") as write_handle:
        write_handle.write(json.dumps(data, indent=3))


    '''
    json_file_in = './raw_json/g2f_2014_hybrid_raw.json'
    json_file_out = './json_clean/g2f_2014_hybrid_clean.json'

    keys = [
        "Date Planted",
        "Date Harvested",
        "Anthesis [date]",
        "Silking [date]",
        "Stand Count [plants]",
        "Pollen DAP [days]",
        "Silk DAP [days]",
        "Plant height [cm]",
        "Ear height [cm]",
        "Root lodging [plants]",
        "Stalk lodging [plants]",
        "Grain Moisture [percent]",
        "Test Weight [lbs/bu]",
        "Plot Weight [lbs]",
        "Grain yield [bu/A]"
    ]

    # All columns for the 2014 hybrid data:
    columns_str = 'RecId,Field-Location,State,City,Code,Source,Pedigree,Experiment,Heterotic Pool,Rep,Block,Plot,Range,Pass,Plot area,Date Planted,Date Harvested,Anthesis [date],Silking [date],Stand Count [plants],Pollen DAP [days],Silk DAP [days],Plant height [cm],Ear height [cm],Root lodging [plants],Stalk lodging [plants],Grain Moisture [percent],Test Weight [lbs/bu],Plot Weight [lbs],Grain yield [bu/A],Plot Discarded,Filler,Comments,Damaged plants,Lodging rating,Percent northern leaf blight,Raccoon damage rating,Plant height [cm to tip of tassel]'
    keys = [k for k in columns_str.split(',')]

    parser = ObservationParser(FILEPATH)
    #parser.get_observations(json_file_in, json_file_out, keys)

    #parser.create_germplasm_fixture('./json_clean/g2f_2014_hybrid_clean.json')
    parser.create_observation_unit_fixture('./json_clean/g2f_2014_hybrid_clean.json')
    '''
