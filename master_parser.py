import sys
import os
import json
from configparser import ConfigParser
from g2f_observation_parser import ObservationParser
import G2fEntityParsers as parsers
from traits_parser import TraitsParser


def skip_fixture(configuration, fixture_key):
    skip = configuration.getboolean('EXCLUSIONS', fixture_key, fallback=False)
    return skip


def parse_raw_csv(input_filepath, output_filepath):

    with open(output_filepath, "w") as write_handle:
        obs_parser = ObservationParser(input_filepath)
        data = obs_parser.read()
        print(f'writing {len(data)} lines to {output_filepath}')
        write_handle.write(json.dumps(data, indent=3))


def make_boilerplate_fixture(configuration):
    print("-" * 40)

    if skip_fixture(configuration, 'Boilerplate'):
        print("SKIPPING BOLERPLATE ENTITIES")
        return


    print("PARSING CROP, ONTOLOGY, OBSERVATION DATA TYPES...")
    boilerplate_output_path = configuration.get('TRIAL', 'BaseOutputPath')
    crop = configuration.get('TRIAL', 'Crop')
    ontology = configuration.get('TRIAL', 'Ontology')

    with open(boilerplate_output_path.format('boilerplate'), 'w') as write_handle:
        fixture = parsers.make_boilerplate_fixtures(crop, ontology)
        write_handle.write(json.dumps(fixture, indent=3))
        print(f"{len(fixture)} misc. fixtures parsed")


def make_studies_fixture(configuration):
    print("-" * 40)
    print("PARSING STUDIES")

    if skip_fixture(configuration, 'Studies'):
        print("SKIPPING STUDIES ENTITIES")
        return


    input_filepath = configuration.get('SOURCE', 'OutputFilePath')
    studies_output_path = configuration.get('TRIAL', 'BaseOutputPath')
    crop = configuration.get('TRIAL', 'Crop')
    ontology = configuration.get('TRIAL', 'Ontology')

    trial_name = configuration.get('TRIAL', 'TrialName')
    study_postfix = configuration.get('TRIAL', 'StudyIdPostfix')
    studies = parsers.studies_parser(input_filepath, crop, trial_name, study_postfix)

    with open(studies_output_path.format('studies'), 'w') as write_handle:
        write_handle.write(json.dumps(studies, indent=3))
        print(f"{len(studies)} studies parsed")

    return studies


def make_trials_fixture(configuration):
    print("-" * 40)

    if skip_fixture(configuration, 'Trials'):
        print("SKIPPING TRIALS ENTITIES")
        return

    print("PARSING TRIALS")

    trials_output_path = configuration.get('TRIAL', 'BaseOutputPath')

    with open(trials_output_path.format('trials'), 'w') as write_handle:
        trials = parsers.trials_parser()
        write_handle.write(json.dumps(trials, indent=3))
        print(f"{len(trials)} trials parsed")


def make_germplasm_fixture(configuration):
    print("-" * 40)
    print("PARSING GERMPLASM")

    germplasm_output_path = configuration.get('TRIAL', 'BaseOutputPath')
    input_filepath = configuration.get('SOURCE', 'OutputFilePath')
    crop = configuration.get('TRIAL', 'Crop')

    with open(germplasm_output_path.format('germplasm'), 'w') as write_handle:

        germplasm = parsers.germplasm_parser(input_filepath, crop)
        write_handle.write(json.dumps(germplasm, indent=3))
        print(f"{len(germplasm)} germplasm parsed")


def make_observation_units_fixture(configuration):
    print("-" * 40)
    print("PARSING OBSERVATION UNITS")

    input_filepath = configuration.get('SOURCE', 'OutputFilePath')
    obs_units_output_path = configuration.get('TRIAL', 'BaseOutputPath').format('obs_units')
    study_postfix = configuration.get('TRIAL', 'StudyIdPostfix')

    with open(obs_units_output_path, 'w') as write_handle:
        obs_units = parsers.observation_units_parser(input_filepath, study_postfix)
        write_handle.write(json.dumps(obs_units, indent=3))
        print(f"{len(obs_units)} observation units parsed")


def make_observations_fixture(configuration):
    print("-" * 40)
    print("PARSING OBSERVATIONS")

    input_filepath = configuration.get('SOURCE', 'OutputFilePath')
    output_filepath = configuration.get('TRIAL', 'BaseOutputPath').format('observations')
    obs_units_output_path = configuration.get('TRIAL', 'BaseOutputPath').format('obs_units')
    crop = configuration.get('TRIAL', 'Crop')


    with open(output_filepath, 'w') as write_handle:
        obs = parsers.observations_parser(input_filepath, crop)
        write_handle.write(json.dumps(obs, indent=3))
        print(f"{len(obs)} observations parsed")


def make_observation_variables_fixture(configuration):
    print("-" * 40)

    if skip_fixture(configuration, 'ObservationVariables'):
        print("SKIPPING OBSERVATION VARIABLES")
        return

    print("PARSING OBSERVATION VARIABLES")

    output_filepath = configuration.get('TRIAL', 'BaseOutputPath').format('observation_variables')

    with open(output_filepath, 'w') as write_handle:
        observation_variables = parsers.observation_variables_parser()
        write_handle.write(json.dumps(observation_variables, indent=3))
        print(f"{len(observation_variables)} observation variables parsed")


def make_traits_fixture(configuration):
    print("-" * 40)

    if skip_fixture(configuration, 'Traits'):
        print("SKIPPING TRAITS")
        return


    print("PARSING TRAITS, METHODS, ETC")

    crop = configuration.get('TRIAL', 'Crop')
    trial_partial_path = configuration.get('TRIAL', 'TrialDirectoryPath')
    output_filepath = f'./fixtures/{trial_partial_path}/traits_'

    traits_parser = TraitsParser(crop, output_filepath)
    traits_parser.parse()


def make_season_fixture(configuration):
    print("-" * 40)

    if skip_fixture(configuration, 'Season'):
        print("SKIPPING OBSERVATION VARIABLES")
        return

    print("PARSING SEASONS")

    output_filepath = configuration.get('TRIAL', 'BaseOutputPath').format('seasons')
    crop = configuration.get('TRIAL', 'Crop')
    year = configuration.get('TRIAL', 'Year')
    seasons = parsers.seasons_parser(crop=crop, year=year)

    with open(output_filepath, 'w') as write_handle:
        write_handle.write(json.dumps(seasons, indent=3))
        print(f"{len(seasons)} observation variables parsed")

    return seasons


def make_study_season_fixture(configuration, seasons, studies):
    # this creates the relational models that connects a study to a season
    print("-" * 40)

    if skip_fixture(configuration, 'StudiesSeasons'):
        print("SKIPPING StudiesSeasons")
        return

    print("PARSING StudiesSeasons")


    entities = []
    output_filepath = configuration.get('TRIAL', 'BaseOutputPath').format('studies_seasons')

    for season in seasons:
        for study in studies:
            entities.append({
                "model": "brapi.StudySeason",
                "fields": {
                    "cropdbid": study.get("fields").get("cropdbid"),
                    "studyDbId": study.get("pk"),
                    "seasonDbId": season.get("fields").get("seasonDbId")
                }
            })

    with open(output_filepath, 'w') as output_handle:
        output_handle.write(json.dumps(entities, indent=3))

    return entities




def main(config_path):

    config = ConfigParser()
    config.read(config_path)

    if len(config.sections()) == 0:
        raise FileNotFoundError(f"Could not find valid .ini file at {config_path}")


    input_path = config.get('SOURCE', 'InputFilePath')
    output_path = config.get('SOURCE', 'OutputFilePath')
    trial = config.get('TRIAL', 'TrialDirectoryPath')

    parse_raw_csv(input_path, output_path)

    try:
        os.mkdir(f'./fixtures/{trial}')
    except FileExistsError:
        pass

    make_boilerplate_fixture(config)
    studies = make_studies_fixture(config)
    make_trials_fixture(config)
    make_germplasm_fixture(config)
    make_traits_fixture(config)
    make_observation_units_fixture(config)
    make_observations_fixture(config)
    make_observation_variables_fixture(config)
    seasons = make_season_fixture(config)

    make_study_season_fixture(configuration=config, seasons=seasons, studies=studies)


if __name__ == "__main__":

    conf_path = sys.argv[-1]

    if conf_path.endswith('.ini'):
        main(conf_path)
    else:
        print("Please provide path to .ini file")

