import sys
import os
from django.core.management import call_command
import django

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
django.setup()


def main(path):
    # Fixtures need to be loaded in a certain order to avoid foreign key constraint problems:
    fixture_files = (
        'boilerplate_fixture.json',
        'trials_fixture.json',
        'germplasm_fixture.json',
        'locations_fixture.json',
        'studies_fixture.json',
        'obs_units_fixture.json',
        'traits_methods_fixture.json',
        'traits_traits_fixture.json',
        'observation_variables_fixture.json',
        'traits_scales_fixture.json',
        'observations_fixture.json',
        'seasons_fixture.json',
        'studies_seasons_fixture.json'
    )
    print(f"TRYING {path}")

    for root, subdirs, files in os.walk(path):

        for file in fixture_files:

            if not file.endswith(".json"):
                continue

            fpath = f"{root}{file}"
            print(f"TRYING {fpath}")

            try:

                with open(fpath, "r") as test_exists_handle:
                    print(f"LOADING {fpath}")
                    call_command('loaddata', fpath)

            except FileNotFoundError:
                print(f"MISSING FIXTURE FILE: {fpath}")
                print("THIS MAY CAUSE FURTHER MAJOR ERRORS")

            print("")


if __name__ == '__main__':

    fixtures_dir = sys.argv[-1]
    main(fixtures_dir)

