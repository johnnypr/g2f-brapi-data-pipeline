import json

''' This file parses data from a local ontology json file downloaded from http://www.cropontology.org/get-ontology/CO_322 '''



class TraitsParser:

    def __init__(self, crop_name, output_path):

        self.crop_name = crop_name
        self.output_path = output_path

        # These are the ontology IDs collected by the undergrads:
        self.traits_str = '''CO_322:0000994 CO_322:0000995	
CO_322:0000017 CO_322:0000179	
CO_322:0001091 CO_322:0001092	
CO_322:0000015 CO_322:0000349	
CO_322:0001091 CO_322:0001092	
CO_322:0000167 CO_322:0000221	
CO_322:0000008 CO_322:0000201	
CO_322:0000179 CO_322:0000569	
CO_322:0000035 CO_322:0000570	
CO_322:0000031 CO_322:0000905 CO_322:0000191
CO_322:0000030 CO_322:0000795 CO_322:0000189
CO_322:0000033 CO_322:0000195	
CO_322:0001005 CO_322:0001006	
CO_322:0000005 CO_322:0000217
CO_322:0000030 CO_322:0000189 CO_322:0000352
CO_322:0000031 CO_322:0000191 CO_322:0000352
CO_322:0001005 CO_322:0001006 CO_322:0001007
CO_322:0000033 CO_322:0000195 CO_322:0000350'''


        self.data_types_map = {
            'Numerical': 1,
            'Numeric': 1,
            'Categorical': 2,
            'Date': 3,
            'Time': 3,
            'Text': 4,
            'Picture': 5,
            'Boolean': 6,
        }

        #PROJECT = 'g2f_2015_hybrid'
        self.ontology_filepath = './ontology_data/CO_322.json'


    def parse(self):

        with open(self.ontology_filepath, 'r') as ontology_handle:

            ontology = json.loads(ontology_handle.read())
            trait_keys = ontology.keys()
            #OUTPUT_PATH = f'./fixtures/{PROJECT}/'


            output_traits = {}
            output_methods = {}
            output_units_of_meas = {}


            for line in self.traits_str.split('\n'):
                line_split = line.strip().split(" ")
                trait_id = line_split[0]
                method_id = line_split[1]

                if trait_id in trait_keys:

                    trait_def = ontology.get(trait_id)

                    if not trait_def:
                        print(f"no trait def found! {trait_id}")
                        continue

                    for k, v in trait_def.get('method').items():

                        if k not in output_methods:

                            output_methods[k] = {
                                "model": "brapi.method",
                                "pk": k,
                                "fields": {
                                    "cropdbid": "maize",
                                    "name": v.get("name").get("english"),
                                    "description": v.get("definition").get("english"),
                                    #"classis":
                                }
                            }

                            scales = v.get("scale")

                            for scale_id, scale in scales.items():
                                if scale_id not in output_units_of_meas:

                                    output_units_of_meas[scale_id] = {
                                        "model": "brapi.scale",
                                        "pk": scale_id,
                                        "fields": {
                                            "cropdbid": "maize",
                                            "name": scale.get("scale_name").get("english"),
                                            #"type": scale.get("type").get("english"),
                                            'datatypeDbId': self.data_types_map[scale.get("type").get("english")]

                                        }
                                    }

                    has_synonym = bool(trait_def.get('synonym') and trait_def.get('synonym').get('english'))

                    if has_synonym:
                        synonym = trait_def.get('synonym').get('english')
                    else:
                        synonym = ''

                    output_traits[trait_id] = {
                        "model": "brapi.trait",
                        "pk": trait_id
                        , "fields": {
                              "cropdbid": "maize"
                              , "traitId": trait_id
                              , "defaultValue": ""
                              , "name": trait_def.get('name').get('english')
                              , "description": trait_def.get('definition').get('english')
                              , "classis": trait_def.get('is_a').get('english') #"Agronomical trait"
                              , "synonyms": '' #trait_def.get('synonym').get('english', '')
                              #, "mainAbbreviation": "PH"
                        }
                    }

            # Finally, write out the json fixture files for scales, methods and traits:
            with open(self.output_path + "methods_fixture.json", "w") as fixture_handle:
                fixture_handle.write(json.dumps([v for k,v in output_methods.items()], indent=3))

            with open(self.output_path + "scales_fixture.json", "w") as fixture_handle:
                fixture_handle.write(json.dumps([v for k,v in output_units_of_meas.items()], indent=3))

            with open(self.output_path + "traits_fixture.json", "w") as fixture_handle:
                fixture_handle.write(json.dumps([v for k,v in output_traits.items()], indent=3))


if __name__ == '__main__':

    test = TraitsParser('maize', './fixtures/test/')
    test.parse()
